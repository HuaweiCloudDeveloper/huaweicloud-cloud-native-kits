# 仓库简介<a name="ZH-CN_TOPIC_0000001380785708"></a>

云原生服务中心（Operator Service Center，OSC）是面向服务提供商和服务使用者的云原生服务生命周期治理平台，提供大量的云原生服务，并使用自研部署引擎，支持所有服务包统一管理、统一存储、全域分发，帮助您简化云原生服务的生命周期管理。

UCS深度集成云原生服务中心的功能，可真正实现服务的开箱即用，有效提升云原生服务能力与质量，支持服务的订阅、部署、升级、更新等操作。借助UCS的跨云跨集群的管理能力，云原生服务中心突破了服务部署的地域限制，部署范围不再局限于中心Region，线下IDC和第三方服务商集群同样可实现实例分发，真正做到服务实例一键统一分发部署。

# 项目总览<a name="ZH-CN_TOPIC_0000001431544665"></a>

<a name="table8497184001110"></a>
<table><thead align="left"><tr id="row1449814402112"><th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.1.4.1.1"><p id="p3498204091119"><a name="p3498204091119"></a><a name="p3498204091119"></a>项目</p>
</th>
<th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.1.4.1.2"><p id="p13498114011115"><a name="p13498114011115"></a><a name="p13498114011115"></a>简介</p>
</th>
<th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.1.4.1.3"><p id="p16498194018119"><a name="p16498194018119"></a><a name="p16498194018119"></a>仓库</p>
</th>
</tr>
</thead>
<tbody><tr id="row9887144714118"><td class="cellrowborder" rowspan="2" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p1660322061220"><a name="p1660322061220"></a><a name="p1660322061220"></a>服务网格</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1860382031219"><a name="p1860382031219"></a><a name="p1860382031219"></a>基于CCE部署Istio官方Demo</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p1560362010121"><a name="p1560362010121"></a><a name="p1560362010121"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-istio-bookinfo-development-guide" target="_blank" rel="noopener noreferrer">huaweicloud-istio-bookinfo-development-guide</a></p>
</td>
</tr>
<tr id="row54613484111"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p106033205121"><a name="p106033205121"></a><a name="p106033205121"></a>SaaS-housekeeper istio改造实践</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.1.4.1.2 "><p id="p8603142017129"><a name="p8603142017129"></a><a name="p8603142017129"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-istio-k8s-saas-housekeeper" target="_blank" rel="noopener noreferrer">huaweicloud-istio-k8s-saas-housekeeper</a></p>
</td>
</tr>
<tr id="row0560248161111"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p060322013127"><a name="p060322013127"></a><a name="p060322013127"></a>业务部署</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p8604172071210"><a name="p8604172071210"></a><a name="p8604172071210"></a>将Workflow样例部署到华为云CCI服务</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p1560415203120"><a name="p1560415203120"></a><a name="p1560415203120"></a><a href="https://gitee.com/HuaweiCloudDeveloper/deploy-cci-workflow-sample" target="_blank" rel="noopener noreferrer">deploy-cci-workflow-sample</a></p>
</td>
</tr>
<tr id="row1064917481111"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p176041620181211"><a name="p176041620181211"></a><a name="p176041620181211"></a>镜像部署</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1460410206127"><a name="p1460410206127"></a><a name="p1460410206127"></a>将镜像部署到华为云容器实例</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p8604220121219"><a name="p8604220121219"></a><a name="p8604220121219"></a><a href="https://gitee.com/HuaweiCloudDeveloper/deploy-cci-action" target="_blank" rel="noopener noreferrer">deploy-cci-action</a></p>
</td>
</tr>
<tr id="row1375654841116"><td class="cellrowborder" rowspan="2" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p86044202129"><a name="p86044202129"></a><a name="p86044202129"></a>业务部署</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1360414204128"><a name="p1360414204128"></a><a name="p1360414204128"></a>将业务部署到华为云CCE的docker节点</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p18604102018125"><a name="p18604102018125"></a><a name="p18604102018125"></a><a href="https://gitee.com/HuaweiCloudDeveloper/deploy-cce-workflow-sample" target="_blank" rel="noopener noreferrer">deploy-cce-workflow-sample</a></p>
</td>
</tr>
<tr id="row1485015487111"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p96041320141218"><a name="p96041320141218"></a><a name="p96041320141218"></a>将业务部署到华为云CCE的k8s服务</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.1.4.1.2 "><p id="p46043209123"><a name="p46043209123"></a><a name="p46043209123"></a><a href="https://gitee.com/HuaweiCloudDeveloper/deploy-ccek8s-workflow-sample" target="_blank" rel="noopener noreferrer">deploy-ccek8s-workflow-sample</a></p>
</td>
</tr>
<tr id="row20945348171116"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p0604520101220"><a name="p0604520101220"></a><a name="p0604520101220"></a>CCE集群部署环境准备</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p660410207125"><a name="p660410207125"></a><a name="p660410207125"></a>安装kubectl，获取并设置CCE集群kubeconfig</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p1604122031211"><a name="p1604122031211"></a><a name="p1604122031211"></a><a href="https://gitee.com/HuaweiCloudDeveloper/cce-credential-action" target="_blank" rel="noopener noreferrer">cce-credential-action</a></p>
</td>
</tr>
<tr id="row175154912114"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p0604192031211"><a name="p0604192031211"></a><a name="p0604192031211"></a>SQL加速方案</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p116042020121219"><a name="p116042020121219"></a><a name="p116042020121219"></a>基于云搜索服务的SQL加速</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p560472091212"><a name="p560472091212"></a><a name="p560472091212"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-search-acceleration-based-css" target="_blank" rel="noopener noreferrer">huaweicloud-solution-search-acceleration-based-CSS</a></p>
</td>
</tr>
<tr id="row725412492119"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p16604152019125"><a name="p16604152019125"></a><a name="p16604152019125"></a>VPC对等连接</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p76041620151212"><a name="p76041620151212"></a><a name="p76041620151212"></a>快速实现不同VPC之间的流量互通</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p116051520201219"><a name="p116051520201219"></a><a name="p116051520201219"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-peer-to-peer-connection-based-cross-vpc-network-mutual-access" target="_blank" rel="noopener noreferrer">huaweicloud-solution-peer-to-peer-connection-based-cross-VPC-network-mutual-access</a></p>
</td>
</tr>
<tr id="row2064914918113"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p7605920131218"><a name="p7605920131218"></a><a name="p7605920131218"></a>SaaS版CMS一键部署</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p17605820151213"><a name="p17605820151213"></a><a name="p17605820151213"></a>基于华为云应用编排私有化一键部署SaaS版CMS</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p660542031218"><a name="p660542031218"></a><a name="p660542031218"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms" target="_blank" rel="noopener noreferrer">huaweicloud-solution-build-wangmarketcms</a></p>
</td>
</tr>
<tr id="row107421549191113"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p126051920101214"><a name="p126051920101214"></a><a name="p126051920101214"></a>一键部署雷鸣IM系统</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1560552031215"><a name="p1560552031215"></a><a name="p1560552031215"></a>基于华为云应用编排，一键部署雷鸣云客服IM系统</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p1460512021217"><a name="p1460512021217"></a><a name="p1460512021217"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-kefu-im" target="_blank" rel="noopener noreferrer">huaweicloud-solution-build-kefuIM</a></p>
</td>
</tr>
<tr id="row159461449141117"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p136051720171217"><a name="p136051720171217"></a><a name="p136051720171217"></a>书籍评论</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1560512017128"><a name="p1560512017128"></a><a name="p1560512017128"></a>结合OSC展示其产品的服务部署、订阅和运维等能力</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p7605142016121"><a name="p7605142016121"></a><a name="p7605142016121"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-OSC-helm-bookinfo-reviews-sample" target="_blank" rel="noopener noreferrer">huaweicloud-OSC-helm-bookinfo-reviews-sample</a></p>
</td>
</tr>
<tr id="row163361850161111"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p20606112061217"><a name="p20606112061217"></a><a name="p20606112061217"></a>CCE接口调用</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p66061520141215"><a name="p66061520141215"></a><a name="p66061520141215"></a>获取Kubernetes集群中的信息</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p2606152091218"><a name="p2606152091218"></a><a name="p2606152091218"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cce-SDK-samples" target="_blank" rel="noopener noreferrer">huaweicloud-cce-SDK-samples</a></p>
</td>
</tr>
<tr id="row84431350161114"><td class="cellrowborder" rowspan="2" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p760662012125"><a name="p760662012125"></a><a name="p760662012125"></a>部署CAE DEMO应用</p>
</td>
<td class="cellrowborder" rowspan="2" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p11606720191217"><a name="p11606720191217"></a><a name="p11606720191217"></a>体验使用CAE serverless化托管运维应用、弹性伸缩和一站式组件配置等能力</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p4606520141213"><a name="p4606520141213"></a><a name="p4606520141213"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cae-frontend" target="_blank" rel="noopener noreferrer">huaweicloud-cae-frontend</a></p>
</td>
</tr>
<tr id="row18540205018114"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p36061820171212"><a name="p36061820171212"></a><a name="p36061820171212"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cae-backend" target="_blank" rel="noopener noreferrer">huaweicloud-cae-backend</a></p>
</td>
</tr>
<tr id="row7835650151120"><td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.1 "><p id="p196071420151211"><a name="p196071420151211"></a><a name="p196071420151211"></a>java代码调用华为云服务示例</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p166071220131212"><a name="p166071220131212"></a><a name="p166071220131212"></a>让用户更为方便的使用华为云各类产品及服务</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.3 "><p id="p360782041214"><a name="p360782041214"></a><a name="p360782041214"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-segment-scenes-samples" target="_blank" rel="noopener noreferrer">huaweicloud-basic-scenario-development-samples</a></p>
</td>
</tr>
</tbody>
</table>

